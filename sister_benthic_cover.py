#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SISTER
Space-based Imaging Spectroscopy and Thermal PathfindER
Author: Winston Olson-Duvall
"""

import json
import os
import shutil
import subprocess
import sys

import numpy as np

from osgeo import gdal
from PIL import Image


def get_benthcov_basename(benthrfl_basename, crid):
    # Replace product type
    tmp_basename = benthrfl_basename.replace("L2B_BENTHRFL", "L2B_BENTHCOV")
    # Split, remove old CRID, and add new one
    tokens = tmp_basename.split("_")[:-1] + [str(crid)]
    return "_".join(tokens)


def generate_metadata(run_config, json_path, new_metadata):

    metadata = run_config['metadata']
    for key, value in new_metadata.items():
        metadata[key] = value
    with open(json_path, 'w') as out_obj:
        json.dump(metadata, out_obj, indent=4)


def convert_to_geotiff_and_png(envi_path, basename, band_names, units, descriptions):

    in_file = gdal.Open(envi_path)

    # Temporary geotiff
    temp_file = f"work/{basename}_tmp.tif"

    # Set the output raster transform and projection properties
    driver = gdal.GetDriverByName("GTIFF")
    tiff = driver.Create(temp_file,
                         in_file.RasterXSize,
                         in_file.RasterYSize,
                         4,
                         gdal.GDT_Float32)

    tiff.SetGeoTransform(in_file.GetGeoTransform())
    tiff.SetProjection(in_file.GetProjection())

    # Dataset description
    tiff.SetMetadataItem("DESCRIPTION", "BENTHIC COVER")

    # Write bands to file
    bands = []
    for i, band_name in enumerate(band_names):
        in_band = in_file.GetRasterBand(i + 1)
        out_band = tiff.GetRasterBand(i + 1)
        bands.append(in_band.ReadAsArray())
        out_band.WriteArray(in_band.ReadAsArray())
        out_band.SetDescription(band_name)
        out_band.SetNoDataValue(in_band.GetNoDataValue())
        out_band.SetMetadataItem("UNITS", units[i])
        out_band.SetMetadataItem("DESCRIPTION", descriptions[i])
    del tiff, driver

    # Save as cloud optimized geotiff
    cog_file = f"output/{basename}.tif"
    os.system(f"gdaladdo -minsize 900 {temp_file}")
    os.system(f"gdal_translate {temp_file} {cog_file} -co COMPRESS=LZW -co TILED=YES -co COPY_SRC_OVERVIEWS=YES")

    # Generate quicklook
    ql_path = f"output/{basename}.png"
    print(f"Generating quicklook to {ql_path}")

    # Get nodata from last in_band
    no_data = in_band.GetNoDataValue()
    rgb = np.array(bands)[:3]
    rgb[rgb == no_data] = np.nan

    rgb = np.moveaxis(rgb, 0, -1).astype(float)
    rgb = (rgb * 255).astype(np.uint8)

    im = Image.fromarray(rgb)
    im.save(ql_path)


def main():
    """
        This function takes as input the path to an inputs.json file and exports a run config json
        containing the arguments needed to run the SISTER ISOFIT PGE.

    """
    in_file = sys.argv[1]

    # Read in runconfig
    print("Reading in runconfig")
    with open(in_file, "r") as f:
        run_config = json.load(f)

    # Make work dir
    print("Making work directory")
    if not os.path.exists("work"):
        subprocess.run("mkdir work", shell=True)

    # Make output dir
    print("Making output directory")
    if not os.path.exists("output"):
        subprocess.run("mkdir output", shell=True)

    # Define paths and variables
    sister_benth_cov_pge_dir = os.path.abspath(os.path.dirname(__file__))
    benthcover_dir = os.path.join(os.path.dirname(sister_benth_cov_pge_dir), "sister-benthcover")

    benthrfl_basename = None
    for file in run_config["inputs"]["file"]:
        if "benthic_reflectance_dataset" in file:
            benthrfl_basename = os.path.basename(file["benthic_reflectance_dataset"])

    benthcov_basename = get_benthcov_basename(benthrfl_basename, run_config["inputs"]["config"]["crid"])

    benthrfl_envi_path = f"input/{benthrfl_basename}/{benthrfl_basename}.bin"
    benthrfl_hdr_path = f"input/{benthrfl_basename}/{benthrfl_basename}.hdr"

    # Temporary input filenames without .bin extension
    tmp_benthrfl_envi_path = f"work/{benthrfl_basename}"
    tmp_benthrfl_hdr_path = f"work/{benthrfl_basename}.hdr"


    # Copy the input files into the work directory (don't use .bin)
    shutil.copyfile(benthrfl_envi_path, tmp_benthrfl_envi_path)
    shutil.copyfile(benthrfl_hdr_path, tmp_benthrfl_hdr_path)

    # Get temporary output file names
    tmp_benthcov_envi_path = f"work/{benthrfl_basename}_cover_prob"

    log_path = f"output/{benthcov_basename}.log"

    # Run benthic cover
    benthcover_exe = f"{benthcover_dir}/benthcover.py"
    cmd = [
        "python",
        benthcover_exe,
        tmp_benthrfl_envi_path,
        "work",
        "--prob",
        "--verbose",
        ">>",
        log_path
    ]
    print("Running benthcover command: " + " ".join(cmd))
    subprocess.run(" ".join(cmd), shell=True)

    # Convert to GeoTIFF and PNG
    band_names = ["algae", "coral", "mud/sand", "seagrass"]
    units = ["PERCENT", "PERCENT", "PERCENT", "PERCENT"]
    descriptions = ['ALGAE PERCENT COVER', 'CORAL PERCENT COVER', 'MUD/SAND PERCENT COVER', 'SEAGRASS PERCENT COVER']
    convert_to_geotiff_and_png(tmp_benthcov_envi_path, benthcov_basename, band_names, units, descriptions)

    # Generate metadata
    print("Generating metadata in .met.json files")
    generate_metadata(run_config,
                      f"output/{benthcov_basename}.met.json",
                      {'product': 'BENTHCOV',
                       'processing_level': 'L2B',
                       'description': "Benthic cover (algae, coral, mud/sand, seagrass)"})

    # Copy any remaining files to output
    print("Copying runconfig to output folder")
    shutil.copyfile("runconfig.json", f"output/{benthcov_basename}.runconfig.json")


if __name__ == "__main__":
    main()
