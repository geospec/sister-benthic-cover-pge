# SISTER Benthic Cover PGE Documentation

## Description

The sister-benthic-cover-pge repository is a wrapper for a 
[benthic cover classification algorithm](https://github.com/EnSpec/sister-benthcover) that classifies 
benthic reflectance into four cover classes:
* Algae
* Coral
* Mud/sand
* Seagrass

## Dependencies

This repository is built to run on SISTER (Space-based Imaging Spectroscopy and Thermal pathfindER), a data 
processing back-end that allows for the registration of algorithms as executable containers and execution of those 
containers at scale.  The manifest file that configures this repository for registration and describes all of its 
necessary dependencies is called `algorithm_config.yaml`.  In this file you will find:

* The repository URL and version to register
* The base Docker image which this repository gets installed into, and a reference to its Dockerfile
* The build script which is used to install this repository into the base Docker image

Specific dependencies for executing the code in this repository can be found in both the Dockerfile and the build 
script.

In addition to the above dependencies, you will need access to the MAAP API via the maap-py library in order to 
register algorithms and submit jobs.  maap-py can be obtained by running:

    git clone --single-branch --branch sister-dev https://gitlab.com/geospec/maap-py.git

## PGE Arguments

The sister-benthic-cover-pge PGE takes the following arguments:


| Argument                    | Type   | Description                                      | Default |
|-----------------------------|--------|--------------------------------------------------|---------|
| benthic_reflectance_dataset | file   | S3 URL to the benthic reflectance dataset folder | -       |
| depth_dataset               | file   | S3 URL to the depth dataset folder               | -       |
| crid                        | config | Composite Release ID to tag file names           | 000     |

## Outputs

The L2B benthic cover PGE outputs Cloud-Optimized GeoTIFFs (COGs) and associated metadata and ancillary files. The 
outputs of the PGE use the following naming convention:

    SISTER_INSTRUMENT_LEVEL_PRODUCT_YYYYMMDDTHHMMSS_CRID.EXTENSION

| Product                      | Format, Units                    | Example filename                                            |
|------------------------------|----------------------------------|-------------------------------------------------------------|
| Benthic cover classification | Cloud-Optimized GeoTIFF, Percent | SISTER_AVNG_L2B_BENTHCOV_20210604T090303_000.tif            |
| Benthic cover metadata       | JSON                             | SISTER_AVNG_L2B_BENTHCOV_20210604T090303_000.met.json       |
| Benthic cover browse image   | PNG                              | SISTER_AVNG_L2B_BENTHCOV_20210604T090303_000.png            |
| PGE log file                 | Text                             | SISTER_AVNG_L2B_BENTHCOV_20210604T090303_000.log            |
| PGE run config               | JSON                             | SISTER_AVNG_L2B_BENTHCOV_20210604T090303_000.runconfig.json |

## Registering the Repository with SISTER

    from maap.maap import MAAP
    
    maap = MAAP(maap_host="34.216.77.111")
    
    algo_config_path = "sister-benthic-cover-pge/algorithm_config.yaml"
    response = maap.register_algorithm_from_yaml_file(file_path=algo_config_path)
    print(response.text)

## Submitting a Job on SISTER

    from maap.maap import MAAP
    
    maap = MAAP(maap_host="34.216.77.111")
    
    response = maap.submitJob(
        algo_id="sister-benthic-cover-pge",
        version="1.0.0",
        benthic_reflectance_dataset="s3://s3.us-west-2.amazonaws.com:80/sister-ops-workspace/LOM/PRODUCTS/AVNG/L2B_BENTHRFL/2021/06/04/SISTER_AVNG_L2B_BENTHRFL_20210604T090303_999",
        depth_dataset="s3://s3.us-west-2.amazonaws.com:80/sister-ops-workspace/LOM/PRODUCTS/AVNG/L2B_BENTHRFL/2021/06/04/SISTER_AVNG_L2B_BENTHRFL_20210604T090303_999_DEPTH",
        crid="000",
        publish_to_cmr=False,
        cmr_metadata={},
        queue="sister-job_worker-16gb",
        identifier="WO_BC_20230405_AVNG_1")
    
    print(response.id, response.status)
