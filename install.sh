pge_dir=$(cd "$(dirname "$0")" ; pwd -P)
app_dir=$(dirname ${pge_dir})

pushd $app_dir
git clone https://github.com/EnSpec/sister-benthcover.git -b 1.1.0

# Create conda env
conda create -n benthcover -y -c conda-forge python=3.8 gdal=3.1
source activate benthcover

pushd sister-benthcover

pip install -r requirements.txt
pip install Pillow==9.2.0
