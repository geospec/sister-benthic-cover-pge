#!/bin/bash

# Description:
#
# The top-level run script to execute the sister-benthcover PGE on SISTER (Space-based Imaging Spectroscopy
# and Thermal PathfindER).
#
# File inputs:
#
# Config inputs:
#
# Positional inputs:
#

# Use benthcover conda env from docker image
source activate benthcover

# Get repository directory
REPO_DIR=$(cd "$(dirname "$0")"; pwd -P)

# Generate runconfig
python ${REPO_DIR}/generate_runconfig.py inputs.json

# Execute benthic cover
python ${REPO_DIR}/sister_benthic_cover.py runconfig.json
